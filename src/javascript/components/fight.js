import { controls } from '../../constants/controls';
import { keyboardEvents } from '../../constants/events';
import { players } from '../../constants/players';

const FIGHT_OVER_EVENT = 'fight-over-event';
const keysPressed = {};
const hasCriticalHit = {
  [players.PlayerOne]: true,
  [players.PlayerTwo]: true
};
const CRITICAL_HIT_TIMEOUT = 10000;

export async function fight(firstFighter, secondFighter) {
  const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
  const secondFighterHealthBar = document.getElementById('right-fighter-indicator');
  const firstFighterHealth = firstFighter.health;
  const secondFighterHealth = secondFighter.health;

  return new Promise((resolve) => {
    const params = {
      firstFighter,
      secondFighter,
      firstFighterHealthBar,
      secondFighterHealthBar,
      firstFighterHealth,
      secondFighterHealth
    };
    const hitEventListener = hitListener.bind(this, params);

    document.addEventListener(keyboardEvents.KeyDown, hitEventListener);
    document.addEventListener(keyboardEvents.KeyUp, (event) => {
      keysPressed[event.code] = false;
    });

    document.addEventListener(FIGHT_OVER_EVENT, (event) => {
      resolve(event.detail.winner);
    });
  });
}

export function getCriticalDamage(attacker) {
  const damage = attacker.attack * 2;

  return damage;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return Math.max(damage, 0);
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomIntInclusive(1, 2);
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomIntInclusive(1, 2);
  const power = fighter.defense * dodgeChance;

  return power;
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.random() * (max - min) + min; // Максимум и минимум не включаются
}

function updateFighterHealth({ health, damage }) {
  return Math.max(health - damage, 0);
}

function updateFighterHealthBar(fighter, healthBar, initialHealth) {
  const currentPercent = 100 * fighter.health / initialHealth; // 100% * currentHealth / initialHealth
  const updatedWith = Math.max(currentPercent, 0);

  healthBar.style.width = `${updatedWith}%`;
}

function hitListener(params, event) {
  // prevent holding keys
  if (event.repeat) {
    return;
  }
  keysPressed[event.code] = true;

  if (playerOneCriticalAttack()) {
    hasCriticalHit[players.PlayerOne] = false;
    enableCriticalHit(players.PlayerOne);
    handleAction({
      attacker: params.firstFighter,
      defender: params.secondFighter,
      defenderHealth: params.secondFighterHealth,
      defenderHealthBar: params.secondFighterHealthBar,
      damageFn: getCriticalDamage
    });
  } else if (playerTwoCriticalAttack()) {
    hasCriticalHit[players.PlayerTwo] = false;
    enableCriticalHit(players.PlayerTwo);
    handleAction({
      attacker: params.secondFighter,
      defender: params.firstFighter,
      defenderHealth: params.firstFighterHealth,
      defenderHealthBar: params.firstFighterHealthBar,
      damageFn: getCriticalDamage
    });
  } else if (playerOneAttackThroughBlock()) {
    handleAction({
      attacker: params.firstFighter,
      defender: params.secondFighter,
      defenderHealth: params.secondFighterHealth,
      defenderHealthBar: params.secondFighterHealthBar,
      damageFn: getDamage,
      blockPower: Number.POSITIVE_INFINITY
    });
  } else if (playerTwoAttackThroughBlock()) {
    handleAction({
      attacker: params.secondFighter,
      defender: params.firstFighter,
      defenderHealth: params.firstFighterHealth,
      defenderHealthBar: params.firstFighterHealthBar,
      damageFn: getDamage,
      blockPower: Number.POSITIVE_INFINITY
    });
  } else if (playerOneAttack()) {
    handleAction({
      attacker: params.firstFighter,
      defender: params.secondFighter,
      defenderHealth: params.secondFighterHealth,
      defenderHealthBar: params.secondFighterHealthBar,
      damageFn: getDamage,
      blockPower: getBlockPower(params.secondFighter)
    });
  } else if (playerTwoAttack()) {
    handleAction({
      attacker: params.secondFighter,
      defender: params.firstFighter,
      defenderHealth: params.firstFighterHealth,
      defenderHealthBar: params.firstFighterHealthBar,
      damageFn: getDamage,
      blockPower: getBlockPower(params.firstFighter)
    });
  }
}

function handleAction({ attacker, defender, defenderHealth, defenderHealthBar, damageFn, blockPower = 0 }) {
  const dmg = damageFn(attacker, defender);
  const damage = Math.max((dmg - blockPower), 0);
  defender.health = updateFighterHealth({
    health: defender.health,
    damage
  });
  updateFighterHealthBar(defender, defenderHealthBar, defenderHealth);
  if (defender.health === 0) {
    const fightOverEvent = new CustomEvent(FIGHT_OVER_EVENT, {
      detail: { winner: attacker }
    });
    document.dispatchEvent(fightOverEvent);
  }
}

function enableCriticalHit(fighter) {
  setTimeout(() => {
    hasCriticalHit[fighter] = true;
  }, CRITICAL_HIT_TIMEOUT);
}

function playerOneAttack() {
  return keysPressed[controls.PlayerOneAttack] && !keysPressed[controls.PlayerOneBlock];
}

function playerOneAttackThroughBlock() {
  return keysPressed[controls.PlayerOneAttack] && !keysPressed[controls.PlayerOneBlock] &&
    keysPressed[controls.PlayerTwoBlock];
}

function playerOneCriticalAttack() {
  return controls.PlayerOneCriticalHitCombination.every(key => keysPressed[key]) &&
    hasCriticalHit[players.PlayerOne] && !keysPressed[controls.PlayerOneBlock];
}

function playerTwoAttack() {
  return keysPressed[controls.PlayerTwoAttack] && !keysPressed[controls.PlayerTwoBlock];
}

function playerTwoAttackThroughBlock() {
  return keysPressed[controls.PlayerTwoAttack] && !keysPressed[controls.PlayerTwoBlock] &&
    keysPressed[controls.PlayerOneBlock];
}

function playerTwoCriticalAttack() {
  return controls.PlayerTwoCriticalHitCombination.every(key => keysPressed[key]) &&
    hasCriticalHit[players.PlayerTwo] && !keysPressed[controls.PlayerTwoBlock];
}
