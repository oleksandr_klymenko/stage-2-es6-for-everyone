import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  bodyElement.textContent = `${fighter.name} 🥊 this round.`;

  showModal({
    title: 'Winner🏆🎉!!!',
    bodyElement,
  });
}
