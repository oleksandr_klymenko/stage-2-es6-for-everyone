import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  try {
    const imgElement = createFighterImage(fighter);
    const nameElement = createFighterName(fighter);
    const statsElement = createFighterStats(fighter);

    fighterElement.append(imgElement);
    // fighterElement.append(nameElement);
    fighterElement.append(statsElement);
  } catch (error) {
    console.warn(error);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

export function createFighterName(fighter) {
  const { name } = fighter;
  const attributes = {
    'data-content': name
  };
  const divElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___name',
    attributes
  });
  divElement.textContent = name;

  return divElement;
}

export function createFighterStats(fighter) {
  const { health, attack, defense } = fighter;
  const divElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___stats',
  });
  const healthElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info fighter-preview___health',
  });
  const attackElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info fighter-preview___attack',
  });
  const defenceElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info fighter-preview___defence',
  });

  healthElement.textContent = health;
  attackElement.textContent = attack;
  defenceElement.textContent = defense;

  divElement.append(healthElement);
  divElement.append(attackElement);
  divElement.append(defenceElement);

  return divElement;
}
